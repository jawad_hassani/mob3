// @flow
import * as React from "react";
import { connect } from "react-redux";
import { Container, Header, Title, ListItem, List, Content, Text, Right, Left, Body} from "native-base";
import { FileSystem } from "expo";
import { ListView } from "react-native";
import LoadingContainer from "../LoadingContainer";
import { NavigationActions } from "react-navigation";

import styles from "./styles";
import * as actions from "./actions";

export interface Props {
	navigation: any,
	files: Array,
}
export interface State {}

class DataContainer extends React.Component<Props, State> {

	render () {
		const {
			navigation,
			dataReducer,
			filesDataSource,
			nav,
		} = this.props;

		const { goBack } = this.props.navigation;
		
		if (dataReducer.isLoading) {
			return <LoadingContainer/>;
		}
		if (dataReducer.isInvalidated) {
			this.props.dispatch(actions.openFile(FileSystem.documentDirectory + navigation.state.params.file));
			return <LoadingContainer/>;
		}

		return (
			// TODO: Empty view, check if files length == 0
			<Container>
				<Header >
        	<Left>
        		<Button transparent title="Back" onPress={() => goBack(null)} >
							<Icon name='ios-arrow-back' />
						</Button>
        	</Left>
					<Body>
						<Title>File rows</Title>
					</Body>
					<Right />
				</Header>
				<Content>
					<List
						dataSource={filesDataSource}
						renderRow={data =>
							<ListItem onPress={() => navigation.navigate("SelectChart", { row: data })}>
								<Text style={styles.align}> {data} </Text>
							</ListItem>
						}
						renderLeftHiddenRow={() => null}
						renderRightHiddenRow={() => null}
						disableRightSwipe={true}
						disableLeftSwipe={true}
					/>
				</Content>
			</Container>
		);
	}
}

const dataSource = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2,
});

const mapStateToProps = (state) => ({
	filesDataSource: dataSource.cloneWithRows(state.dataReducer.fileList),
	files: state.dataReducer.fileList,
	dataReducer: state.dataReducer,
	nav: state.nav,
});
export default connect(mapStateToProps)(DataContainer);
