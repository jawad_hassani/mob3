import { FileSystem } from "expo";

export function parseFile(fileContent: string) {
  const arrayRows = fileContent.split("\n");
  const arrayColumns = arrayRows[1].split("\t");
  arrayColumns.shift();

  return {
    type: "PARSE_FILE",
    fileList: arrayColumns,
    openedFile: fileContent,
  };
}

export function openFile(uri: string) {
  return (dispatch) => {
    dispatch({ type: "READ_FILE" });
    return FileSystem.readAsStringAsync(uri)
      .then((file) => dispatch(parseFile(file: string)));
  };
}
