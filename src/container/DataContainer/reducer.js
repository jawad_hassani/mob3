const initialState = {
	fileList: [],
	openedFile: null,
  isFetching: false,
  isInvalidated: true,
};

export default function(state: any = initialState, action: Function) {
	if (action.type === "READ_FILE") {
		return {
			...state,
			isLoading: true,
			isInvalidated: false,
		};
	}
	if (action.type === "PARSE_FILE") {
		return {
			...state,
			fileList: action.fileList,
			openedFile: action.openedFile,
			isLoading: false,
			isInvalidated: false,
		};
  }
  if (action.type === "INVALIDATE_FILE") {
    return {
      fileList: [],
      isFetching: false,
      isInvalidating: true,
		};
  }
	return state;
}
