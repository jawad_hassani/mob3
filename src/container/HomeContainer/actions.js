import { FileSystem } from "expo";

export function updateList(files: array) {
	return {
		type: "UPDATE_LIST",
		files,
	};
}

export function addFile(file: string) {
  return {
    type: "ADD_FILE",
    file,
  };
}

export function fetchList(uri: string) {
  return (dispatch) => {
    dispatch({ type: "READ_LIST" });
    return FileSystem.readDirectoryAsync(uri)
      .then((files) => dispatch(updateList(files: array)));
  };

}

