const initialState = {
	files: [],
	isLoading: false,
	isInvalidated: true,
};

export default function(state: any = initialState, action: Function) {
	if (action.type === "READ_LIST") {
		return {
			...state,
			isLoading: true,
			isInvalidated: false,
		};
	}
	if (action.type === "UPDATE_LIST") {
		return {
			...state,
			files: action.files,
			isLoading: false,
			isInvalidated: false,
		};
	}
	if (action.type === "ADD_FILE") {
		return {
			...state,
			files: state.files.concat(action.file),
		};
	}
	return state;
}
