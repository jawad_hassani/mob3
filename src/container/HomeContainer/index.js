// @flow
import * as React from "react";
import { connect } from "react-redux";
import { Container, Header, Title, ListItem, List, Content, Text, Button, Icon, Right, Left, Body} from "native-base";
import { DocumentPicker, FileSystem } from "expo";
import { ListView } from "react-native";
import { NavigationActions } from "react-navigation";

import LoadingContainer from "../LoadingContainer";

import styles from "./styles";
import * as actions from "./actions";

export interface Props {
	navigation: any,
	fetchList: Function,
	pickDocument: Function,
	deleteDocument: Function,
	files: Array,
}
export interface State {}

class HomeContainer extends React.Component<Props, State> {
	constructor(props) {
		super(props);
    this.pickDocument = this.pickDocument.bind(this);
    this.deleteDocument = this.deleteDocument.bind(this);
	}

	async pickDocument() {
		const file = await DocumentPicker.getDocumentAsync({ type: "text/*" });
		if (file.type === "cancel") {return null;}
		const fileContent = await FileSystem.downloadAsync(file.uri, FileSystem.documentDirectory + new Date().toISOString() + "_" + file.name);
		const uploadedFile = await FileSystem.readAsStringAsync(fileContent.uri);
		if (uploadedFile) { return await this.props.dispatch(actions.addFile(new Date().toISOString() + "_" + file.name)); }
	}

	async deleteDocument(secId, rowId, rowMap) {
		rowMap[`${secId}${rowId}`].props.closeRow();
		const newData = this.props.files;
		const itemToDelete = newData.splice(rowId, 1);
		await FileSystem.deleteAsync(FileSystem.documentDirectory + itemToDelete);
		return await this.props.dispatch(actions.updateList(newData));
	}

	render() {
		const {
			filesDataSource,
			isLoading,
			isInvalidated,
			dispatch,
		} = this.props;

		if (isLoading) {
			return <LoadingContainer/>;
		}
		if (isInvalidated) {
			dispatch(actions.fetchList(FileSystem.documentDirectory));
			return <LoadingContainer/>;
		}

		return (
			// TODO: Empty view, check if files length == 0
			<Container>
				<Header >
					<Left/>
					<Body>
						<Title>Files List</Title>
					</Body>
					<Right>
						<Button transparent title="Select Document" onPress={this.pickDocument}>
							<Icon name='download' />
						</Button>
					</Right>
				</Header>
				<Content>
					<List
						dataSource={filesDataSource}
						renderRow={data =>
							<ListItem
								onPress={() => dispatch(NavigationActions.navigate({ routeName: "Data", params: {file: data }}))}
							>
								<Icon active name="ios-document-outline" style={styles.iconFile} />
								<Text style={styles.align}>{data}</Text>
							</ListItem>
						}
						renderLeftHiddenRow={() => null}
						renderRightHiddenRow={(data, secId, rowId, rowMap) =>
							<Button full danger onPress={_ => this.deleteDocument(secId, rowId, rowMap)}>
								<Icon active name="trash" />
							</Button>
						}
						disableRightSwipe={true}
						rightOpenValue={-75}
					/>
				</Content>
			</Container>
		);
	}
}

const dataSource = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2,
});

const mapStateToProps = state => ({
	filesDataSource: dataSource.cloneWithRows(state.homeReducer.files),
	files: state.homeReducer.files,
	isLoading: state.homeReducer.isLoading,
	isInvalidated: state.homeReducer.isInvalidated,
});
export default connect(mapStateToProps)(HomeContainer);
