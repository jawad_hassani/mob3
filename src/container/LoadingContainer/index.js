// @flow
import { createElement as $ } from "react";
import { Container, Header, Title, Content, Text, Right, Body } from "native-base";

import styles from "./styles";

export interface Props {}
export interface State {}

const LoadingContainer = (props) => {
  return (
    $(Container, { style: styles.container },
      $(Header, null,
        $(Body, null,
          $(Title, null, "Loading"),
        ),
        $(Right, null),
      ),
      $(Content, { padder: true },
        $(Text, null, "Loading"),
      ),
    )
  );
};

export default (LoadingContainer);
