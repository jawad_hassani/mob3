// @flow
import * as React from "react";
import { connect } from "react-redux";
import { Container, Header, Title, ListItem, List, Content, Text, Button, Icon, Right, Left, Body} from "native-base";
import { ListView } from "react-native";

import styles from "./styles";

const list = ["Basic Line", "Line time series", "Polar wind rose"];
const dataSource = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2,
});

export interface Props {
	navigation: any,
	fetchList: Function,
	pickDocument: Function,
	deleteDocument: Function,
	files: Array,
}
export interface State {}

class SelectChartsContainer extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      list: dataSource.cloneWithRows(list),
    };
  }

	render () {
		const { navigation } = this.props;
		const { goBack } = this.props.navigation;
		return (
			<Container>
				<Header >
				<Left>
						<Button transparent title="Back" onPress={() => goBack(null)} >
								<Icon name='ios-arrow-back' />
						</Button>
        	</Left>
					<Body>
						<Title>Select chart</Title>
					</Body>
					<Right />
				</Header>
				<Content>
					<List
						dataSource={this.state.list}
						renderRow={data =>
							<ListItem onPress={() => navigation.navigate("Chart", { chartType: data })}>
								<Text style={styles.align}>{data}</Text>
							</ListItem>
						}
						renderLeftHiddenRow={() => null}
						renderRightHiddenRow={() => null}
						disableRightSwipe={true}
						disableLeftSwipe={true}
					/>
				</Content>
			</Container>
		);
	}
}

export default connect()(SelectChartsContainer);
