import { StackNavigator } from "react-navigation";

import Home from "../container/HomeContainer";
import Data from "../container/DataContainer";
import SelectCharts from "../container/SelectChartsContainer";
import Charts from "../container/ChartsContainer";

export default StackNavigator({
	Home: { screen: Home },
	Data: { screen: Data },
	SelectChart: { screen: SelectCharts },
	Chart: { screen: Charts }
},
{
	initialRouteName: "Home",
	headerMode: "none",
});
