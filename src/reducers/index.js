import { combineReducers } from "redux";
import AppNavigator from "../boot/appNavigator";
import homeReducer from "../container/HomeContainer/reducer";
import dataReducer from "../container/DataContainer/reducer";

const initialState = AppNavigator.router.getStateForAction(AppNavigator.router.getActionForPathAndParams("Home"));
const navReducer = (state = initialState, action) => {
  const nextState = AppNavigator.router.getStateForAction(action, state);
  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
};

export default combineReducers({
	nav: navReducer,
	homeReducer,
	dataReducer,
});
